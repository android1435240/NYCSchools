package com.nycschools.cityschools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.nycschools.cityschools.data.SchoolRepository
import com.nycschools.cityschools.model.SchoolDetailsModel
import com.nycschools.cityschools.model.SchoolModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository
) : ViewModel() {

    val citySchoolsHashMap = liveData(Dispatchers.IO) {
        val fetchedSchools : List<SchoolModel> = schoolRepository.getSchools()
        emit(fetchedSchools)
    }

    val schoolDetails = liveData(Dispatchers.IO) {
        val fetchedSchools : List<SchoolDetailsModel> = schoolRepository.getSchoolDetails()
        emit(fetchedSchools)
    }
}