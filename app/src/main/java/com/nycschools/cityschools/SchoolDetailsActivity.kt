package com.nycschools.cityschools

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.nycschools.cityschools.model.SchoolDetailsModel
import com.nycschools.cityschools.model.SchoolModel
import com.nycschools.cityschools.viewmodel.MainViewModel
import javax.inject.Inject

class SchoolDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var schoolDetailsViewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as MyApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_details)
        val abnView: TextView = findViewById(R.id.abn)
        val nameView: TextView = findViewById(R.id.name)
        val addressView: TextView = findViewById(R.id.address)
        val cityView: TextView = findViewById(R.id.city)
        val zipView: TextView = findViewById(R.id.zip)
        val stateView: TextView = findViewById(R.id.stateCode)
        val webView: TextView = findViewById(R.id.webSite)
        val phoneView: TextView = findViewById(R.id.phoneNumber)
        val testTakersView: TextView = findViewById(R.id.testTakers)
        val readingAvgScoreView: TextView = findViewById(R.id.readingAvgScore)
        val mathAvgScoreView: TextView = findViewById(R.id.mathAvgScore)
        val writingAvgScoreView: TextView = findViewById(R.id.writingAvgScore)

        val school: SchoolModel? = intent.extras?.getSerializable("schoolDetails") as SchoolModel?
        abnView.text = school?.dbn
        nameView.text = school?.school_name
        addressView.text = school?.primary_address_line_1
        cityView.text = school?.city
        zipView.text = school?.zip
        stateView.text = school?.state_code
        webView.text = school?.website
        phoneView.text = school?.phone_number

        schoolDetailsViewModel.schoolDetails.observe(
            this,
            Observer<List<SchoolDetailsModel>> { list ->
                list.forEach {
                    if (it.dbn?.equals(school?.dbn) == true) {
                        testTakersView.text = it?.numOfSatTestTakers.toString()
                        readingAvgScoreView.text = it?.satCriticalReadingAvgScore.toString()
                        mathAvgScoreView.text = it?.satMathAvgScore.toString()
                        writingAvgScoreView.text = it?.satWritingAvgScore.toString()
                    }
                }
            })
    }
}