package com.nycschools.cityschools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nycschools.cityschools.R
import com.nycschools.cityschools.model.SchoolModel


class SchoolListAdapter(private val mList: List<SchoolModel>, val onItemClicked: (SchoolModel) -> Unit) : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_card_layout, parent, false)
        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val schoolModel = mList[position]
        holder.textView.text = schoolModel.school_name
        holder.title.text = schoolModel.location
        holder.itemView.setOnClickListener {
            onItemClicked(schoolModel)
        }
    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.textView)
        val title: TextView = itemView.findViewById(R.id.title)
    }
}