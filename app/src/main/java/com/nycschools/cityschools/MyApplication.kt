package com.nycschools.cityschools

import android.app.Application
import com.nycschools.cityschools.data.RetrofitBuilder
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitBuilder::class])
interface ApplicationComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(schoolDetailsActivity: SchoolDetailsActivity)
}

class MyApplication: Application() {
    val appComponent = DaggerApplicationComponent.create()
}