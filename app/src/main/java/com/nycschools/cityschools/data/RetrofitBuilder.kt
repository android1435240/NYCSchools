package com.nycschools.cityschools.data

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitBuilder {

    @Singleton
    @Provides
    fun provideCitySchoolsApi(): SchoolListApi {
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(SchoolListApi::class.java)
    }
}