package com.nycschools.cityschools.data

import com.nycschools.cityschools.model.SchoolDetailsModel
import com.nycschools.cityschools.model.SchoolModel
import retrofit2.http.GET

interface SchoolListApi {
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getData(): List<SchoolModel>


    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSchoolDetails(): List<SchoolDetailsModel>

}