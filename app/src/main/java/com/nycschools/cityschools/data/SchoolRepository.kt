package com.nycschools.cityschools.data

import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private var schoolListApi: SchoolListApi
) {
    suspend fun getSchools() = schoolListApi.getData()

    suspend fun getSchoolDetails() = schoolListApi.getSchoolDetails()
}