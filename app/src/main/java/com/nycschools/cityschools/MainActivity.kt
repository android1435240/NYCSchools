package com.nycschools.cityschools

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nycschools.cityschools.adapter.SchoolListAdapter
import com.nycschools.cityschools.model.SchoolModel
import com.nycschools.cityschools.viewmodel.MainViewModel
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var mainViewModel: MainViewModel
    lateinit var infoTextView: TextView
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: SchoolListAdapter
    val data = ArrayList<SchoolModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as MyApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        infoTextView = findViewById(R.id.infoTextView)
        recyclerView = findViewById(R.id.recyclerview)

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = SchoolListAdapter(data, ::onSchoolItemClicked)
        recyclerView.adapter = adapter

        mainViewModel.citySchoolsHashMap.observe(
            this,
            Observer<List<SchoolModel>> { list ->
                hideLoader(list.isNotEmpty())
                populateExpandableListView(list)
            })
    }

    private fun hideLoader(dataFound: Boolean) {
        if (dataFound) {
            infoTextView.visibility = View.GONE
        } else {
            infoTextView.text = "No data found"
        }
    }

    private fun populateExpandableListView(schoolModelList: List<SchoolModel>) {
        data.addAll(schoolModelList)
        adapter.notifyDataSetChanged()
    }

    private fun onSchoolItemClicked(schoolModel: SchoolModel) {
        val intent = Intent(this, SchoolDetailsActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("schoolDetails", schoolModel)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}